import React from 'react';
import { View , Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Router , Scene , Lightbox, Drawer} from 'react-native-router-flux';

EStyleSheet.build({
    $statusBarColor : '#2c3e50',
    $headerColor : '#34495e',
    $IS : 'IRANSansMobile'
})

// Components

import Login from "./components/Login";
import Home from "./components/Home";
import FoodsList from "./components/FoodsList";
import Splash from './components/Splash';
import sideBar from './components/sideBar';
import orders from './components/orders';
import daySelections from './components/daySelections';
import verifyCode from './components/verifyCode';
import Wallet from './components/wallet'
import ForcastResult from './components/forcastResult'
import Pardakht from './components/pardakht';
import Test from './components/foodCardStruct'

class loginLightbox extends React.Component {
    render() {
        return (
            <View style={{ flex : 1 , justifyContent: 'center' , alignItems: 'center'}}>
                <Text>lightBox</Text>
            </View>
        )
    }
}

export default class App extends React.Component {
    render() {
        return (
            <Router>
                <Scene hideNavBar>
                    <Scene key="root" hideNavBar >
                      <Drawer
                      drawerPosition="right"
                      key="drawer"
                      contentComponent={sideBar}>
                      
                       <Scene hideNavBar>
                        
                        <Scene key="FoodsList" component={FoodsList} />
                        <Scene key="daySelections" component={daySelections}  />
                        <Scene key="orders" component={orders} />
                        <Scene key="verifyCode" component={verifyCode}  />
                        <Scene key="login" component={Login} />
                        <Scene key="wallet" component={Wallet} />
                        <Scene key="forcastResult" component={ForcastResult} />
                        <Scene key="pardakht" component={Pardakht} />
                        <Scene key="Test" component={Test} /> 


                       
                       </Scene>  

                       </Drawer>
                    </Scene>

                    <Lightbox key="auth">
                        <Scene key="login" component={Login} />
                       
                    </Lightbox>

                    {/* <Scene key="splash" component={Splash} initial/> */}
                    <Scene key="splash" component={Splash} initial />
                    
               </Scene> 
            </Router>
        )
    }
}