import React from 'react';
import { Container , Header , Right , Button,ScrollableTab , Content,Tabs,Tab , Text } from 'native-base';
import { form } from './../assets/styles';
import { Actions } from 'react-native-router-flux';

import BarnameHaftegy from './tabs/barnameHaftegy';
import ListDorors from './tabs/listDorors';
import NomreMovaghat from './tabs/nomreMovaghat';
import RizNomre from './tabs/rizNomre';

export default class Home extends React.Component {


 


    render() {


        return (
            <Container>
           
            <Tabs renderTabBar={()=> <ScrollableTab />}>
              <Tab heading="Tab1">
                <BarnameHaftegy />
              </Tab>
              <Tab heading="Tab2">
                <ListDorors />
              </Tab>
              <Tab heading="Tab3">
                <NomreMovaghat />
              </Tab>
              <Tab heading="Tab4">
                <RizNomre />
              </Tab>
            
            </Tabs>
          </Container>
        )
    }
}