import React from 'react';
import { Container , Header  , Text , Left , Button , Right , Content , Form , Item , Icon , Input } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { form } from './../assets/styles';
import { Image ,View,Dimensions,AsyncStorage } from 'react-native';

const urls = require("../libs/urls")
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;



export default class Login extends React.Component {
    
    async Login(){
        fetch(urls.SERVER+urls.studentLogin,{
              method:"POST",
              headers:{
                  "Content-Type":"application/json"
              },
              body:JSON.stringify({
                phoneNumber:this.state.pass,
                studentID:this.state.userName
            })})
            .then((response)=>{
              //  alert(JSON.stringify(response))
                return(response.json())
            })
            .then(async (json)=>{
               // alert(JSON.stringify(json))
                if(json.token === null || json.phoneNumber === null){
                    //alert("Token Not Found")
                    return
                }else{
                    //alert(`Token:${json.token}`)
                    try{
                        await AsyncStorage.setItem('phoneNumber',json.phoneNumber)
                    } catch(error){
                      //  alert("student :::"+error)
                        return
                    }
                    try{
                        await AsyncStorage.setItem('token',json.token)
                    }catch(e){
                      //  alert("token save ::::"+e)
                        return
                    }
                    Actions.verifyCode();
                }
            })                    
    }
    constructor(){
        super();
        this.state={
            userName:'',
            userNameError : '',
            pass : '',
            passError : '',
        }
    }





    validation(text,type){
        alph=/^[a-zA-Z0-9_-]{3,15}$/
        
        pass=/((?=.*\d)(?=.*[a-z]).{8,20})/
        
        if(type == 'userName')
        {
            if(text != '')
            {
                if(alph.test(text))
                {
                    this.setState({
                            userNameError : '',
                            userName : text
                    })
                }  
                else{
                    this.errorText('userName','patternNotCorrect')
                }
            } else{
                this.errorText('userName','inputEmpty')
            }  
        }

        if(type == 'pass')
        {
            if(text != '')
            {
                if(pass.test(text))
                {
                    this.setState({
                        passError : '',
                        pass : text
                    })
                }  
                else{
                    this.errorText('pass','patternNotCorrect')
                }
            } else{
                this.errorText('pass','inputEmpty')
            }  
        }
    }

    errorText(whichInput,errorType){
        if(whichInput == 'userName')
            {
                if(errorType == 'inputEmpty')
                this.setState({
                    userNameError : 'نام کاربری خود را وارد کنید'
                })
                if(errorType == 'patternNotCorrect')
                this.setState({
                    userNameError : ' نام کاربری خود را از بین حروف ' +  ' انگلیسی بزرگ یا کوچک انتخاب کنید'
                })
            }
            if(whichInput == 'pass')
            {
                if(errorType == 'inputEmpty')
                this.setState({
                    passError : ' رمز عبور خود را وارد کنید حداقل شامل 8 کارکتر'
                })
                if(errorType == 'patternNotCorrect')
                this.setState({
                    passError : ' رمز عبور باید شامل حروف انگلیسی و اعداد باشد'
                })
            }
        }


    render() {
        return (
            <Container>
               
               

                <View style={{flex:1}}>

                     
                   <View style={{flex:3,flexDirection:'row',justifyContent:'center',alignItems:'center' , backgroundColor:'#B22D30'}}>

                    <Image 
                         style={{width: 200, height: 120}}
                         source={require('./../assets/images/logo.png')}/>
                     </View>



                     <View style={{flex:6.5,flexDirection:'column', justifyContent:'center', backgroundColor:'#B22D30'}}>
                    <Form style={form.StyleForm} >
                        <Item rounded style={form.item} error={this.state.userNameError != ''}>
                            <Input
                                onChangeText={(text)=>this.validation(text,'userName')}
                                placeholder='شماره دانشجویی خود را وارد کنید'
                                style={form.input}
                            />
                            <Icon active name='person' />
                        </Item>
                        <Text style={form.error}>{this.state.userNameError}</Text>
                        <Item rounded style={form.item} error={this.state.passError != ''}>
                            <Input
                                onChangeText={(text)=>/*this.validation(text,'pass')*/this.setState((prev)=>{
                                    prev["pass"] = text;
                                    return prev;
                                })}
                                
                                placeholder='شماره موبایل خود را وارد کنید'
                                //secureTextEntry={true}
                                style={form.input}
                            />
                            <Icon active name='phone-portrait' />
                        </Item>
                        <Text style={form.error}>{this.state.passError}</Text>
                        
                    </Form>
                    </View>


                   <View style={{flex:0.9 , backgroundColor:'#B22D30',justifyContent:'center'}}>
                    <Button full style={form.submitButton} onPress={() =>this.Login()}>
                            <Text style={form.submitText}>ورود</Text>
                        </Button>

                   </View>
                   
                   </View>
              
            </Container>

        )
    }
}