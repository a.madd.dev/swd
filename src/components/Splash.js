import React from 'react';
import { StatusBar ,Image,AsyncStorage } from 'react-native';
import { Container , Spinner, Text } from 'native-base';
import styles from './../assets/styles';
import {Actions} from 'react-native-router-flux'


const urls  = require("../libs/urls");
export default class Splash extends React.Component {
    constructor(){
        super()
        this.state={
            studentID:"",
            token:""
        }
    }

    async checkUser(studentID,token){
                try{
                    const phoneNumber = await AsyncStorage.getItem('phoneNumber');
                    //alert(phoneNumber)
                    try{
                        const token = await AsyncStorage.getItem('token')
                        if(phoneNumber===null || token ===null){
                            Actions.login()
                        }
                        fetch(urls.SERVER + urls.studentSelf,{
                            method:"POST",
                            headers:{
                                "Content-Type":"application/json"
                            },
                            body:JSON.stringify({
                                phoneNumber:phoneNumber,
                                token:token
                            })
                        })
                        .then(resp=>{
                            return resp.json()
                        })
                        .then(respJson=>{
                            //alert(":::::"+JSON.stringify(respJson))
                            if (respJson.status == 500){
                               // alert("User not exists on server");
                                Actions.login();
                            }else{
                                    if(respJson.token == null || respJson.token == undefined)
                                    AsyncStorage.setItem("credit",respJson.credit.toString()).then(c=>{
                                        //alert(c)
                                    }).catch(e=>{
                                        alert(e)
                                    })
                                
                                Actions.FoodsList();
                                //Actions.login();
                            }
                        }).catch(err=>{
                            //alert("Error:",err)
                            Actions.login();
                        });
                    } catch(error){
                        alert("get token :::"+error);
                        Actions.login();
                        return
                    }
                } catch(error){
                    Actions.login();
                    alert("get StudentID::"+error)
                    return
                }
                
                
        
    }
    async componentWillMount() {
        
        setTimeout(async () => {
            if(false) {
               Actions.reset('root');
                
            } else {
                this.checkUser();
                // Actions.reset('auth');
                //Actions.login()
            }
        })


    

    }

    render() {
        const style = styles.index;
        return (
            <Container style={style.splashContainer}>
                <StatusBar backgroundColor="#B22D30" barStyle="light-content"/>
                <Image 
                style={{width: 200, height: 120}}
                source={require('./../assets/images/logo.png')}/>
                <Spinner />
            </Container>
        )
    }
} 