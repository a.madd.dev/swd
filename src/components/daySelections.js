import React from 'react';
import { Container , Header , Right , Button,ScrollableTab , Content,Tabs,Tab , Text ,ListItem, CheckBox ,Body , Item ,Input   } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Image ,View,Dimensions,Alert,AsyncStorage,ScrollView , KeyboardAvoidingView ,TouchableHighlight } from 'react-native';
import OrderCard from './ordersCard';
import FoodCardtruct from './foodCardStruct';


const urls =  require("../libs/urls")
const date = require ("../libs/date")
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;





export default class daySelections extends React.Component{

    constructor(){
        super();
        this.state={
            foodsList : [],
            price:0,
            showTextBox : false,
            showCheckBox : true,
            manCheck : true,
            wemanCheck : false
        
        }
    }
    componentDidMount(){
        //alert(JSON.stringify(this.props.list))
        this.props.list.map(f=>{
        
        this.setState(prev=>{
            prev.price = prev.price + f.price
            return prev
            })
        })
        //alert(JSON.stringify(this.props.list))
        this.setState(prev=>{
            prev.foodsList = this.props.list;
            //alert(JSON.stringify(prev.foodsList))
            return prev
        })
       // console.warn(this.state.foodList)
    }

     showAlert  (element)  {
        listAfterRemove=[];
        Alert.alert(
            'حذف غذا',
            element + '  را حذف کن' ,
            [
            
              {text: 'باشه', onPress: () => {
                  listAfterRemove = this.state.foodList;

                  listAfterRemove.splice(listAfterRemove.indexOf(element),1);
                  this.setState(prev=>{
                      prev.foodList = listAfterRemove;
                      return prev;
                  }) 
                
              }},

              {text: 'نه', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
         // console.warn(listAfterRemove + "listAfterRemove")
    }
    



    getDate(Date){
        this.setState({
            data:Date,
            //foodName:this.props.foodName
        })
       // console.warn(this.state.foodName)
    }

    componentWillMount(){
        this.setState({
            
            //foodName:this.props.foodName
        })  
    }


    async submit(){
        try{
            const phoneNumber = await AsyncStorage.getItem("phoneNumber")
            try{
                
                const token = await AsyncStorage.getItem("token");
                let foods = []
                fetch(urls.SERVER+urls.purchaseNew,{
                    method:"POST",
                    headers:{
                        "Content-Type":"application/json"
                    },
                    body:JSON.stringify({
                      phoneNumber: phoneNumber,
                      token: token,
                      foods:this.state.foodsList
                    })
                  })
                  .then((response)=>{
                      
                      return response.json()
                    })
                  .then(respJson=>{
                      alert(JSON.stringify(respJson))
                      if(respJson.phoneNumber == null){
                          Actions.wallet({toPay:JSON.stringify(respJson)})
                      }else{
                          alert("پرداخت موفق بود")
                      }
                  })
                  .catch(err=>{
                    alert(">>"+err)
                  })
            }
            catch(err){
                alert(err);
            }
        }catch(e){
            alert(e)
        }
    }
    render(){

        return(

               
                    
                    

                 <View style={{flex:1,flexDirection:'column',backgroundColor:'#F8F8F8'}}>

                      
            
                    
                     
                   
                <View style={{flex:4,flexDirection:'column',backgroundColor:'white',paddingTop:height*0.01}}>


                         
                    <Text style={{borderBottomColor:'black',borderBottomWidth:1,marginRight:width*0.02,marginLeft:width*0.02}}>سبد خرید</Text>
                    
                    <ScrollView>
                        <View style={{flex:0.5 , backgroundColor:'white',marginTop:10 ,paddingLeft : 5 , paddingRight : 5 }}>
                        {
                            this.state.foodsList.map(f=>{
                                
                                return <View><OrderCard name={f.name} price={f.price}/></View>
                            })
                        }


                        </View>

                    </ScrollView>


                </View>  

                <KeyboardAvoidingView  behavior="padding">

                <View style={{height:height*0.4,  flexDirection:'column',paddingTop:20,paddingBottom:10}}>

                    <View style={{flex : 1,maxHeight:height*0.08 , minHeight:height*0.08, flexDirection:'row',marginLeft:10,marginRight:10,borderColor:'black',borderWidth:1}}>
                    <TouchableHighlight style={{flex :1}} onPress={()=>
                                this.setState(prev => {return {showTextBox : !prev.showTextBox}; })}>
                        <View style={{flex :1 , backgroundColor :'white',justifyContent:'center'}} >
                            <Text style={{alignSelf:'center'}}
                                >
                                 کد تخفیف </Text>
                        </View>
                        </TouchableHighlight>


                        <View style={{flex :1.5 , backgroundColor :'white',justifyContent:'center',borderLeftWidth:1,borderRightWidth:1}}>
                            <Text style={{alignSelf:'center'}}>  {this.state.price} تومان</Text>
                        </View>


                        <TouchableHighlight style={{flex :1}} onPress={()=>
                        this.setState(prev => {return {showCheckBox : !prev.showCheckBox};
                         })}>
                        <View style={{flex :1 , backgroundColor :'white',justifyContent:'center'}}>
                            <Text style={{alignSelf:'center'}}  > سالن </Text>
                        </View>
                        </TouchableHighlight>
                        
                    </View>


                    {this.state.showCheckBox &&
                        
                        <View style={{flex:2,paddingRight:10}}>
                            <ListItem style={{justifyContent:'flex-end',alignItems:'flex-end'}}>
                              
                                <Text>سالن اقایان</Text>
                                <CheckBox checked={this.state.manCheck} style={{alignSelf:'flex-end',marginLeft:5}} onPress={()=>
                                 this.setState(prev => {return {manCheck : !prev.manCheck , wemanCheck : !prev.wemanCheck};
                         })}/>
                               
                                
                           
                            </ListItem>

                            <ListItem style={{justifyContent:'flex-end',alignItems:'flex-end'}}>
                               
                                <Text>سالن بانوان</Text>
                                <CheckBox checked={this.state.wemanCheck} style={{alignSelf:'flex-end',marginLeft:5}} onPress={()=>
                                 this.setState(prev => {return {wemanCheck : !prev.wemanCheck , manCheck : !prev.manCheck};
                         })}/>
                              
                               
                                
                            </ListItem>
                        </View> 
                    }

                    { this.state.showTextBox &&         
                        <View style={{flex:1,marginBottom:20 , marginTop: 20}} >

                            <Item style={{borderBottomWidth:2 ,justifyContent:'flex-end',marginRight:25,marginLeft:40}}>
                                
                                <Input   style={{width:width*0.5}} />
                                <Text> کد تخفیف :</Text>
                            </Item>

                        </View>    
                    }
                        
                </View>
                
                    <View style={{ backgroundColor:'#B22D30',justifyContent:'center'}}>
                    
                        <Button full style={{backgroundColor:'#B22D30'}} onPress={()=>{
                            this.submit()
                        }}>
                            <Text style={{color:'white',fontSize:width*0.06,padding:5}}>ثبت</Text>
                        </Button>
                            
                    </View> 

                     </KeyboardAvoidingView>
                    </View>
                   
            

            
        )

        




    }









}