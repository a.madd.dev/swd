import React from 'react';

import { Container , Header  , Text , Left , Button , Right , Content , Form , Item , Icon , Input } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { form } from './../assets/styles';
import { Image ,View,Dimensions,AsyncStorage,ScrollView } from 'react-native';

const urls = require("../libs/urls")
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;



export default class rgister extends React.Component {
    
    register(){
        fetch(urls.SERVER+urls.studentRegister,{
              method:"POST",
              headers:{
                  "Content-Type":"application/json"
              },
              body:JSON.stringify({
                studentID: this.state.studentId,
                email:this.state.email,
                phoneNumber:this.state.phoneNumber,
                password:this.state.pass
              })
            })
            .then((response)=>{
                //alert(JSON.stringify(response))
                return response.json()
            })
            .then(respJson=>{
                alert(JSON.stringify(respJson))
                if (respJson.token === null){
                    alert("error")
                    return
                }else{
                    try{
                        AsyncStorage.setItem("studentID",respJson.studentID);
                        try{
                            AsyncStorage.setItem("token",respJson.token);
                        } catch(err){
                            alert(err)
                        }
                    } catch(err){
                        alert(err)
                    }
                }
                Actions.login();
            })
            .catch(err=>{
              alert(err)
            })
    }
    constructor(){
        super();
        this.state={
            phoneNumber:"",
            phoneNumberError:"",
            userName:'',
            userNameError : '',
            email : '',
            emailError : '',
            pass : '',
            passError : '',
            comPass : '',
            comPassError : '',
            studentId :0,
            studentIdError : '',
        }
    }


    validation(text,type){
        alph=/^[a-zA-Z0-9_-]{3,15}$/
        stuId=/^[0-9]{8}$/
        pass=/((?=.*\d)(?=.*[a-z]).{8,20})/
        email=/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
        if(type == 'userName')
        {
            if(text != '')
            {
                if(alph.test(text))
                {
                    this.setState({
                            userNameError : '',
                            userName : text
                    })
                }  
                else{
                    this.errorText('userName','patternNotCorrect')
                }
            } else{
                this.errorText('userName','inputEmpty')
            }  
        }

        if(type == 'studentId')
        {
            
            if(text != '')
            {
                if(stuId.test(text))
                {
                    this.setState({
                        studentIdError : '',
                        studentId : text
                    })
                }  
                else{
                    this.errorText('studentId','patternNotCorrect')
                    console.warn("patern text");
                }
            } else{
                this.errorText('studentId','inputEmpty')
            }  
        }

        if(type == 'pass')
        {
            if(text != '')
            {
                if(pass.test(text))
                {
                    this.setState({
                        passError : '',
                        pass : text
                    })
                }  
                else{
                    this.errorText('pass','patternNotCorrect')
                }
            } else{
                this.errorText('pass','inputEmpty')
            }  
        }
        if(type == 'comPass')
        {
            if(text != '')
            {
                if(pass.test(text))
                {
                    this.setState({
                        comPassError : '',
                        comPass : text
                    })
                }  
                else{
                    this.errorText('pass','patternNotCorrect')
                }
            } else{
                this.errorText('pass','inputEmpty')
            }  
        }

        if(type == 'email')
        {
            if(text != '')
            {
                if(pass.test(text))
                {
                    this.setState({
                        emailError : '',
                        email : text
                    })
                }  
                else{
                    this.errorText('email','patternNotCorrect')
                }
            } else{
                this.errorText('email','inputEmpty')
            }  
        }
    }

    errorText(whichInput,errorType){
        if(whichInput == 'userName')
            {
                if(errorType == 'inputEmpty')
                this.setState({
                    userNameError : 'نام کاربری خود را وارد کنید'
                })
                if(errorType == 'patternNotCorrect')
                this.setState({
                    userNameError : ' نام کاربری خود را از بین حروف ' +  ' انگلیسی بزرگ یا کوچک انتخاب کنید'
                })
            }

            if(whichInput == 'studentId')
          
            {
                
                if(errorType == 'inputEmpty')
                this.setState({
                    studentIdError : 'شماره دانشجویی خود را وارد کنید'
                })
                if(errorType == 'patternNotCorrect')
                this.setState({
                    studentIdError : ' شماره دانشجویی تنها شامل اعداد میباشد'
                    
                })
            }

            if(whichInput == 'pass')
            {
                if(errorType == 'inputEmpty')
                this.setState({
                    passError : ' رمز عبور خود را وارد کنید حداقل شامل 8 کارکتر'
                })
                if(errorType == 'patternNotCorrect')
                this.setState({
                    passError : ' رمز عبور باید شامل حروف انگلیسی و اعداد باشد'
                })
            }
            if(whichInput == 'comPass')
            {
                if(errorType == 'inputEmpty')
                this.setState({
                    comPassError : ' رمز عبور خود را وارد کنید حداقل شامل 8 کارکتر'
                })
                if(errorType == 'patternNotCorrect')
                this.setState({
                    comPassError : ' رمز عبور باید شامل حروف انگلیسی و اعداد باشد'
                })
            }
       

            if(whichInput == 'email')
            {
                if(errorType == 'inputEmpty')
                this.setState({
                    emailError : ' ایمیل خود را وارد کنید'
                })
                if(errorType == 'patternNotCorrect')
                this.setState({
                    emailError : 'ایمیل شما نا معتبر است'
                })
            }

          


    }



    render() {
        return (
            <Container>
               
               

                <View style={{flex:1}}>

                     
                   <View style={{flex:0.3,flexDirection:'row',justifyContent:'center',alignItems:'center' , backgroundColor:'#B22D30'}}>

                    <Image 
                         style={{width: 200, height: 120}}
                         source={require('./../assets/images/logo.png')}/>
                     </View>
                     


                     View
                     <Form style={form.StyleForm}>
                         {/*  <Item rounded style={form.item} error={this.state.userNameError != ''}>
                                <Input
                                    onChangeText={(text)=>this.validation(text,'userName')}
                                    placeholder='نام کاربری خود را وارد کنید'
                                    style={form.input}
                                />
                                <Icon active name='contact' />
                            </Item>
                            <Text style={form.error}>{this.state.userNameError}</Text>    */}

                            <Item rounded style={form.item} error={this.state.studentIdError != ''} >
                                <Input
                                    onChangeText={(text)=>this.validation(text,'studentId')}
                                    placeholder='شماره موبایل خود را وارد کنید'
                                    style={form.input}
                                />
                                <Icon active name='contact' />
                            </Item>
                            <Text style={form.error}>{this.state.phoneNumberError}</Text>

                            <Item rounded style={form.item} error={this.state.studentIdError != ''} >
                                <Input
                                    onChangeText={(text)=>this.validation(text,'studentId')}
                                    placeholder='شماره دانشجویی خود را وارد '
                                    style={form.input}
                                />
                                <Icon active name='contact' />
                            </Item>
                            <Text style={form.error}>{this.state.studentIdError}</Text>

                         {/*   <Item rounded style={form.item} error={this.state.emailError != ''}>
                                <Input
                                    onChangeText={(text)=>this.validation(text,'email')}
                                    placeholder='ایمیل خود را وارد کنید'
                                    style={form.input}
                                />
                                <Icon active name='md-mail' />
                            </Item>
                            <Text style={form.error}>{this.state.emailError}</Text>
                            <Item rounded style={form.item} error={this.state.passError != ''}>
                                <Input
                                    secureTextEntry={true}
                                    onChangeText={(text)=>this.validation(text,'pass')}
                                    placeholder='پسورد خود را وارد کنید'
                                    style={form.input}
                                />
                                <Icon active name='md-key' />
                            </Item>
                            <Text style={form.error}>{this.state.passError}</Text>
                            <Item rounded style={form.item} error={this.state.comPassError != ''}>
                                <Input
                                    secureTextEntry={true}
                                    onChangeText={(text)=>this.validation(text,'comPass')}
                                    placeholder='پسورد خود را تکرار کنید'
                                    style={form.input}
                                />
                                <Icon active name='md-key' />
                            </Item>
                               <Text style={form.error}>{this.state.comPassError}</Text> */}              
                    </Form>
                


                   <View style={{ backgroundColor:'#B22D30',position:'relative',bottom:0}}>
                    <Button full style={form.submitButton} onPress={() => this.register()}>
                            <Text style={form.submitText}>
                               ثبت نام
                            </Text>
                        </Button>

                   </View>
                   
                   </View>
              
            </Container>

        )
    }
}