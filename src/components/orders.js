import React from 'react';
import {  Container, Header, Content, Card, CardItem, Thumbnail,
    Text, Button, Icon, Left, Body, Right,Tabs,Tab,ScrollableTab,Drawer , List, ListItem,} from 'native-base';
import { Actions } from 'react-native-router-flux';
import {  View, Image,Dimensions,AsyncStorage } from 'react-native';
import OrdersCard from './ordersCard'


const date = require("../libs/date");
const urls = require("../libs/urls");
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
let values = ["soghra","kobra","toba","batoll"];

export default class orders extends React.Component{
    constructor(props){
        super(props)

        this.state={
            days:[],
            purchases:[],            
        }
    }
    async componentDidMount(){
        await this.getPurchases();

    }
    
    async getPurchases(){
      //  alert("@getPurchase")
        try{
            const phoneNumber = await AsyncStorage.getItem("phoneNumber")
            try{
                const token = await AsyncStorage.getItem("token")
                fetch(urls.SERVER+urls.purchaseHistory,{
                    method:"POST",
                    headers:{
                        "Content-Type":"application/json"
                    },
                    body:JSON.stringify({
                      phoneNumber: phoneNumber,
                      token: token,
                    })
                  })
                  .then((response)=>{
                    return response.json()
                  })
                  .then(respJson=>{
                    //  alert("JSON::::"+JSON.stringify(respJson))
                      respJson.map(p=>{
                          this.setState(prev=>{
                            prev.purchases.push(p)
                            return prev
                          })
                      })
                     // alert(JSON.stringify(this.state.purchases))
                  })
                  .catch(err=>{
                  //  alert(err)
                  })
            }catch(e){
               // alert(e)
                
            }
        } catch(e){
           // alert(e)
           // Actions.login()
        }

        
      }
    render(){

        return(
            <Container>
                <Content>
                {
                    this.state.purchases.map(p=>{
                        return(
                            <View>
                                <Text style={{alignSelf: 'center'}}>{p.purchaseDate}</Text>
                                {
                                    p.foodsList.map(f=>{
                                    return <View><OrdersCard name={f.name} price={f.price}/></View>
                                })}
                            </View>
                        )
                    })
                }
                    
                       
                </Content>
            </Container>
        )} }
