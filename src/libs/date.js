const persianDate = require('persian-date');
module.exports={
    getDayName:(ymd)=>{
       let today = new persianDate(ymd);
       return today.format("ddd")
    },
    getTodayDateString:()=>{
        let today = new persianDate(new Date());
        
        return today.format("YY/MM/D");
    },
    getWeekDay:()=>{
        let today = new persianDate(new Date())
        return today.day();
    }
}