module.exports = {
    //SERVER:"http://localhost:8080/",
    SERVER:"http://5.9.171.124:8080/",
    studentLogin:"student/login",
    thisWeek:"purchase/week/get",
    studentVerify:"student/verify",
    studentSelf:"student/get",
    studentGetCredit:"student/get/credit",
    studentAddCredit:"purchase/charge",
    purchaseNew:"purchase/new",
    purchaseHistory:"purchase/history",
    purchaseByRecievingDate:"purchase/recieve_date",
    purchaseByPurchaseDate:"purchase/purchase_date",
    foodByCodeName:"food/code",//append food name
    foodGetAll:"food/all",
    foodByType:"food/type/",//append food type
    foodByKind:"food/kind/",//append food kind
    newBet:"bet/new",
    getGames:"bet/get/games"
}